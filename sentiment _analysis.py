import rospy
from std_msgs.msg import String

#Defining list for sentimental analysis
    positive=["cooperative","divine","dependable","accommodating","Blissful","Jovial","Thrilled"]
    negative=["insipid","Evil","grim","monstrous" ,"Dreadful","Apathy","Insidious","Dreadful"]
    nuetral=["clinical","objective","nostalgic","ok","hello","he","she"]

#function used for sentiment analysis
def sentiment_analysis(data):
    y=data.data

# check for the presence of published data in positive list
    for i in positive:
    
    	if i==y:
        	y="IT IS A POSITIVE WORD"
        	pub = rospy.Publisher('speech', String, queue_size=10)
        	rospy.init_node('listener', anonymous=True)
        	rate = rospy.Rate(10) # 10hz

        	while not rospy.is_shutdown():
       		 
       		 	rospy.loginfo(y)
        	 	pub.publish(y)
        	 	rate.sleep()

# check for the presence of published data in negative list
    for j in negative:
    
    	if j==y:
        	y="IT IS A NEGATIVE WORD"
       		pub = rospy.Publisher('speech', String, queue_size=10)
        	rospy.init_node('listener', anonymous=True)
        	rate = rospy.Rate(10) # 10hz

        	while not rospy.is_shutdown():
       		 
       		 	rospy.loginfo(y)
        	 	pub.publish(y)
        	 	rate.sleep()

# check for the presence of published data in nuetral list
    for k in nuetral:
    
    	if k==y:
        	y="IT IS A NEUTRAL WORD"
        	pub = rospy.Publisher('speech', String, queue_size=10)
        	rospy.init_node('listener', anonymous=True)
        	rate = rospy.Rate(10) # 10hz

       		while not rospy.is_shutdown():
       		 
       		  rospy.loginfo(y)
        	  pub.publish(y)
        	  rate.sleep()
        	
        	
        	
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('listener', anonymous=True)

    rospy.Subscriber('speech', String, sentiment_analysis)

# Spin() simply keeps python from exiting until this node is stopped
    rospy.spin()

# Start of main function
if __name__ == '__main__':
    listener()
    
