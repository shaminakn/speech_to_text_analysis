import rospy
from std_msgs.msg import String

# Requires PyAudio and speech recognition
import speech_recognition as sr

# Function to convert speech received from microphone to text
def speech_to_text():
#Creating publisher node
      pub = rospy.Publisher('speech', String, queue_size=10)
      rospy.init_node('speech_to_text', anonymous=True)
      rate = rospy.Rate(10) # 10hz

# Record Audio
# Microphone is used as the source of speech
      r=sr.Recognizer()
      with sr.Microphone() as source:
	print("say something")
	audio =r.listen(source)

# Input speech is displayed on terminal
      while not rospy.is_shutdown():
          y=r.recognize_google(audio)
          rospy.loginfo(y)
          pub.publish(y)
          rate.sleep()

# Main function  
if __name__ == '__main__':
      try:
          speech_to_text()
      except rospy.ROSInterruptException:
         pass
